package mk.ukim.finki.wp.lab.service;

import mk.ukim.finki.wp.lab.model.Grade;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface GradeService {
    List<Grade> findAll();
    Optional<Grade> findById(Long id);
    Grade changeGrade(Long id, char grade, String username, Long courseId);
    List<Grade> findAllByCourseId(Long courseId);
    Map<String, Character> getGradesWithMap(Long courseId);
    Grade addGrade(String username, Long courseId);
    Grade findByUsernameAndCourseId(String username, Long courseId);
    List<Grade> findAllByUsername(String username);
}
