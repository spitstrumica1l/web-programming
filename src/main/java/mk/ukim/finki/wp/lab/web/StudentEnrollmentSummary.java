package mk.ukim.finki.wp.lab.web;

import mk.ukim.finki.wp.lab.model.Grade;
import mk.ukim.finki.wp.lab.model.exceptions.CourseNotFoundException;
import mk.ukim.finki.wp.lab.service.CourseService;
import mk.ukim.finki.wp.lab.service.GradeService;
import mk.ukim.finki.wp.lab.service.StudentService;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet(name = "StudentEnrollmentSummary", urlPatterns = "/StudentEnrollmentSummary")
public class StudentEnrollmentSummary extends HttpServlet {

    private final StudentService studentService;
    private final SpringTemplateEngine springTemplateEngine;
    private final CourseService courseService;
    private final GradeService gradeService;

    public StudentEnrollmentSummary(CourseService courseService, StudentService studentService, SpringTemplateEngine springTemplateEngine, GradeService gradeService) {
        this.courseService = courseService;
        this.studentService = studentService;
        this.springTemplateEngine = springTemplateEngine;
        this.gradeService = gradeService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        WebContext context = new WebContext(req, resp, req.getServletContext());
        String username = req.getParameter("size");
        Long courseId = Long.parseLong(String.valueOf(req.getSession().getAttribute("courseId")));
        courseService.addStudentInCourse(username, courseId);
//        studentService.addCourseToStudent(username, courseId);
        context.setVariable("course", courseService.findCourseById(courseId)
                .orElseThrow(() -> new CourseNotFoundException(courseId)));
        context.setVariable("grades", this.gradeService.getGradesWithMap(courseId));
        context.setVariable("grade1", this.gradeService.findAll());
        resp.setContentType("text/html");
        springTemplateEngine.process("studentsInCourse.html", context, resp.getWriter());
    }

}
