package mk.ukim.finki.wp.lab.service.impl;

import mk.ukim.finki.wp.lab.model.Course;
import mk.ukim.finki.wp.lab.model.Student;
import mk.ukim.finki.wp.lab.model.exceptions.InvalidArgumentsException;
import mk.ukim.finki.wp.lab.model.exceptions.NoSuchStudentException;
import mk.ukim.finki.wp.lab.model.exceptions.StudentAlreadyPresentException;
import mk.ukim.finki.wp.lab.repository.impl.InMemoryCourseRepository;
//import mk.ukim.finki.wp.lab.repository.impl.InMemoryStudentRepository;
import mk.ukim.finki.wp.lab.repository.jpa.CourseRepository;
import mk.ukim.finki.wp.lab.repository.jpa.StudentRepository;
import mk.ukim.finki.wp.lab.service.StudentService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final CourseRepository courseRepository;

    public StudentServiceImpl(StudentRepository studentRepository, CourseRepository courseRepository) {
        this.studentRepository = studentRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public List<Student> listAll() {
        return studentRepository.findAll();
    }

    @Override
    public List<Student> searchByNameOrSurname(String text) {
        if(text.isEmpty()){
            throw new InvalidArgumentsException();
        }
        List<Student> students = studentRepository.findAllByName(text);
        if(students.isEmpty())
            throw new NoSuchStudentException(text);
        return students;
    }

    @Override
    public Student save(String username, String password, String name, String surname) {
        if(username.isEmpty() || password == null || password.isEmpty()){
            throw new InvalidArgumentsException();
        }
        if(studentRepository.findByUsername(username).isPresent()){
            throw new StudentAlreadyPresentException(username);
        }
        Student student = new Student(username, password, name, surname);
        studentRepository.save(student);
        return student;
    }


    @Override
    public Student findStudentByUsername(String username) {
        return studentRepository.findByUsername(username).orElseThrow(() -> new NoSuchStudentException(username));
    }
}
