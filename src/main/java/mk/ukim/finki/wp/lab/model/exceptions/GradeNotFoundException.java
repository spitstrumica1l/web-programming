package mk.ukim.finki.wp.lab.model.exceptions;

public class GradeNotFoundException extends RuntimeException{
    public GradeNotFoundException(Long id) {
        super(String.format("Grade with id %d was not found", id));;
    }
}
