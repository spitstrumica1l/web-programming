package mk.ukim.finki.wp.lab.web.rest;

import mk.ukim.finki.wp.lab.model.Course;

import mk.ukim.finki.wp.lab.service.CourseService;
import mk.ukim.finki.wp.lab.service.TeacherService;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController("RestCourseController")
@RequestMapping("/rest")
public class CourseController {

    private final CourseService courseService;


    public CourseController(CourseService courseService, TeacherService teacherService) {
        this.courseService = courseService;
    }

    @GetMapping
    public List<Course> getCoursesPage(){
        return  courseService.listAllCourses().stream().sorted().collect(Collectors.toList());
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteById(@PathVariable Long id){
        this.courseService.deleteById(id);
        if (this.courseService.findCourseById(id).isEmpty() || !this.courseService.findCourseById(id).isPresent()) return ResponseEntity.ok().build();
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/addcourse")
    public Course saveCourse(@RequestParam String name,
                                     @RequestParam String description,
                                     @RequestParam Long teacher,
                                     @RequestParam(required = false) Long courseId){
        return this.courseService.saveCourse(courseId, name, description, teacher);
    }

    @PostMapping("/addstudent")
    public Course addStudentToCourse(@RequestParam String username,
                                     @RequestParam Long courseId){
        return courseService.addStudentInCourse(username, courseId);
    }
}
