package mk.ukim.finki.wp.lab.model.exceptions;

public class NoSuchStudentException extends RuntimeException{

    public NoSuchStudentException(String text){
            super(String.format("There is no such student with name or surname %s", text));
        }

}
