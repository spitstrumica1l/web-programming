package mk.ukim.finki.wp.lab.repository.impl;

import mk.ukim.finki.wp.lab.model.Course;
import mk.ukim.finki.wp.lab.model.Student;
import mk.ukim.finki.wp.lab.model.Teacher;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class InMemoryCourseRepository {

    List<Course> courseList = new ArrayList<>();

    @PostConstruct
    public void init(){
        courseList.add(new Course("Web programming", "web programming course"));
        courseList.add(new Course("Advanced programming", "advanced programming course"));
        courseList.add(new Course("Andorid programming", "andorid programming course"));
        courseList.add(new Course("iOS programming", "iOS programming course"));
        courseList.add(new Course("Data bases", "data bases course"));
    }

    public List<Course> findAllCourses(){
        return courseList;
    }

    public Optional<Course> findById(Long courseId){
        return courseList
                .stream()
                .filter(r -> r.getCourseId().equals(courseId))
                .findFirst();
    }

    public Optional<Course> findByName(String name){
        return courseList
                .stream()
                .filter(i -> i.getName().equals(name))
                .findFirst();
    }

    public List<Student> findAllStudentsByCourse(Long courseId){
        Course course = courseList.stream().filter(r->r.getCourseId().equals(courseId)).findFirst().get();
        return course.getStudents();
    }

    public Course addStudentToCourse(Student student, Course course){
        course.getStudents().removeIf(r->r.getUsername().equals(student.getUsername()));
        course.getStudents().add(student);
        return course;
    }

    public Optional<Course> save(Course course){
        this.courseList.removeIf(i -> i.getCourseId().equals(course.getCourseId()));
        this.courseList.add(course);
        return Optional.of(course);
    }

    public boolean deleteById(Long id) {
        return this.courseList.removeIf(i -> i.getCourseId().equals(id));
    }
}
