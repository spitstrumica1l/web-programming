package mk.ukim.finki.wp.lab.service.impl;

import mk.ukim.finki.wp.lab.model.Course;
import mk.ukim.finki.wp.lab.model.Student;
import mk.ukim.finki.wp.lab.model.Teacher;
import mk.ukim.finki.wp.lab.model.exceptions.*;
import mk.ukim.finki.wp.lab.repository.jpa.CourseRepository;
import mk.ukim.finki.wp.lab.repository.jpa.StudentRepository;
import mk.ukim.finki.wp.lab.service.CourseService;
import mk.ukim.finki.wp.lab.service.TeacherService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;
    private final StudentRepository studentRepository;
    private final TeacherService teacherService;

    public CourseServiceImpl(CourseRepository courseRepository, StudentRepository studentRepository, TeacherService teacherService) {
        this.courseRepository = courseRepository;
        this.studentRepository = studentRepository;
        this.teacherService = teacherService;
    }

    @Override
    public List<Student> listStudentsByCourse(Long courseId) {
        return courseRepository.findAllByCourseId(courseId);
    }

    @Override
    public Course addStudentInCourse(String username, Long courseId) {
        Course course = courseRepository.findById(courseId).orElseThrow(InvalidStudentOrCourseException::new);
        Student student = studentRepository.findByUsername(username).orElseThrow(InvalidStudentOrCourseException::new);
        course.getStudents().removeIf(s -> s.getUsername().equals(username));
        course.getStudents().add(student);
        courseRepository.save(course);
        return course;
    }

    @Override
    public List<Course> listAllCourses() {
        return courseRepository.findAll();
    }

    @Override
    public Optional<Course> findCourseById(Long courseId) {
        return Optional.ofNullable(courseRepository.findById(courseId).orElseThrow(() -> new CourseNotFoundException(courseId)));
    }

    @Override
    public Course saveCourse(Long id,String name, String opis, Long teacherId) {
        Teacher teacher = this.teacherService.findById(teacherId)
                .orElseThrow(() -> new TeacherNotFoundException(teacherId));
        if(id != null) {
            Course course = this.courseRepository.findById(id).orElseThrow(() -> new CourseNotFoundException(id));
            course.setName(name);
            course.setDescription(opis);
            course.setTeacher(teacher);
            return this.courseRepository.save(course);
        }
        else {
            if (this.courseRepository.findByName(name).isPresent())
                throw new CourseAlreadyExistsException(name);
            return this.courseRepository.save(new Course(name, opis, teacher));
        }
    }

    @Override
    public void deleteById(Long id) {
        this.courseRepository.deleteById(id);
    }

    @Override
    public List<Course> findAllCoursesForStudent(Student student) {
        return this.courseRepository.findAllByStudentsContaining(student);
    }


}
