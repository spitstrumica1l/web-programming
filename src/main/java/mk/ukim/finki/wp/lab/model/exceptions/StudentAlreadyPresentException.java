package mk.ukim.finki.wp.lab.model.exceptions;

public class StudentAlreadyPresentException extends RuntimeException{
    public StudentAlreadyPresentException(String username) {
        super(String.format("Student with username %s already exists", username));
    }
}
