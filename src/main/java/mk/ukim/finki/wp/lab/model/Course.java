package mk.ukim.finki.wp.lab.model;

import lombok.Data;
import mk.ukim.finki.wp.lab.model.enumeration.Type;
import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Course implements Comparable<Course>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long courseId;

    @Enumerated(EnumType.STRING)
    private Type type;

    private String name;

    private String description;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<Student> students;

    @ManyToOne
    private Teacher teacher;

    public Course() {
        this.students = new ArrayList<>();
    }

    public Course(String name, String description) {
        this.name = name;
        this.description = description;
        this.students = new ArrayList<>();
    }

    public Course(String name, String description, Teacher teacher) {
        this.name = name;
        this.description = description;
        this.students = new ArrayList<>();
        this.teacher = teacher;
    }

    @Override
    public int compareTo(Course o) {
        return name.compareToIgnoreCase(o.name);
    }
}
