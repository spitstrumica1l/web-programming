package mk.ukim.finki.wp.lab.repository.impl;

import mk.ukim.finki.wp.lab.model.Teacher;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class InMemoryTeacherRepository {

    List<Teacher> teachers = new ArrayList<>();

    @PostConstruct
    public void init(){
        teachers.add(new Teacher("Riste", "Stojanov"));
        teachers.add(new Teacher("Gjorgji", "Madjarov"));
        teachers.add(new Teacher("Igor", "Mishkovski"));
        teachers.add(new Teacher("Sasho", "Gramatikov"));
        teachers.add(new Teacher("Milosh", "Jovanovikj"));
    }

    public List<Teacher> findAll(){
        return this.teachers;
    }

    public Optional<Teacher> findById(Long id){
        return this.teachers.stream().filter(i -> i.getId().equals(id)).findFirst();
    }
}
