package mk.ukim.finki.wp.lab.repository.jpa;

import mk.ukim.finki.wp.lab.model.Grade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GradeRepository extends JpaRepository<Grade, Long> {
    List<Grade> findAllByCourseCourseId(Long courseId);
    List<Grade> findAllByStudentUsername(String username);
    Optional<Grade> findByStudentUsernameAndCourseCourseId(String username, Long courseId);
}
