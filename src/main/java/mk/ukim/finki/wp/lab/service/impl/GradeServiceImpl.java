package mk.ukim.finki.wp.lab.service.impl;

import mk.ukim.finki.wp.lab.model.Course;
import mk.ukim.finki.wp.lab.model.Grade;
import mk.ukim.finki.wp.lab.model.Student;
import mk.ukim.finki.wp.lab.model.exceptions.CourseNotFoundException;
import mk.ukim.finki.wp.lab.model.exceptions.GradeNotFoundException;
import mk.ukim.finki.wp.lab.repository.jpa.GradeRepository;
import mk.ukim.finki.wp.lab.service.CourseService;
import mk.ukim.finki.wp.lab.service.GradeService;
import mk.ukim.finki.wp.lab.service.StudentService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class GradeServiceImpl implements GradeService{

    private final GradeRepository gradeRepository;
    private final CourseService courseService;
    private final StudentService studentService;

    public GradeServiceImpl(GradeRepository gradeRepository, CourseService courseService, StudentService studentService) {
        this.gradeRepository = gradeRepository;
        this.courseService = courseService;
        this.studentService = studentService;
    }

    @Override
    public List<Grade> findAll() {
        return this.gradeRepository.findAll();
    }

    @Override
    public Optional<Grade> findById(Long id) {
        return this.gradeRepository.findById(id);
    }

    @Override
    public Grade changeGrade(Long id, char g, String username, Long courseId) {
        System.out.println("blabla");
        if(this.gradeRepository.findById(id).isPresent()){
            Grade grade = this.gradeRepository.findById(id).orElseThrow(()-> new GradeNotFoundException(id));
            grade.setGrade(g);
            return this.gradeRepository.save(grade);
        }
        else {
            Course course = this.courseService.findCourseById(courseId).orElseThrow(()->new CourseNotFoundException(courseId));
            Student student = this.studentService.findStudentByUsername(username);
            return this.gradeRepository.save(new Grade(g, student, course));
        }
    }

    @Override
    public List<Grade> findAllByCourseId(Long courseId) {
        return this.gradeRepository.findAllByCourseCourseId(courseId);
    }

    @Override
    public Map<String, Character> getGradesWithMap(Long courseId) {
        Map<String, Character> hashMap = new HashMap<>();
        List<Grade> grades = findAllByCourseId(courseId);
        grades.forEach(grade -> hashMap.put(grade.getStudent().getUsername(), grade.getGrade()));
        return hashMap;
    }

    @Override
    public Grade addGrade(String username, Long courseId) {
        Course course = this.courseService.findCourseById(courseId).orElseThrow(()->new CourseNotFoundException(courseId));
        Student student = this.studentService.findStudentByUsername(username);
        return this.gradeRepository.save(new Grade(student, course));
    }

    @Override
    public Grade findByUsernameAndCourseId(String username, Long courseId) {
        return this.gradeRepository.findByStudentUsernameAndCourseCourseId(username, courseId).orElseThrow(()-> new GradeNotFoundException(courseId));
    }

    @Override
    public List<Grade> findAllByUsername(String username) {
        return this.gradeRepository.findAllByStudentUsername(username);
    }

}
