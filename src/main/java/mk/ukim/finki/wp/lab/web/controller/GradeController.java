package mk.ukim.finki.wp.lab.web.controller;

import mk.ukim.finki.wp.lab.model.Grade;
import mk.ukim.finki.wp.lab.model.Student;
import mk.ukim.finki.wp.lab.model.exceptions.CourseNotFoundException;
import mk.ukim.finki.wp.lab.service.CourseService;
import mk.ukim.finki.wp.lab.service.GradeService;
import mk.ukim.finki.wp.lab.service.StudentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class GradeController {

    private final GradeService gradeService;
    private final CourseService courseService;
    private final StudentService studentService;

    public GradeController(GradeService gradeService, CourseService courseService, StudentService studentService) {
        this.gradeService = gradeService;
        this.courseService = courseService;
        this.studentService = studentService;
    }

    @GetMapping("/grades")
    public String getGradesPage(Model model){
        model.addAttribute("students", this.studentService.listAll());
        model.addAttribute("courses", this.courseService.listAllCourses());
        return "grades";
    }

    @PostMapping("/grades")
    public String postGradesPage(Model model,
                                 @RequestParam(required = false) Long courseId,
                                 @RequestParam(required = false) String username){
        if(courseId != null){
            model.addAttribute("grades",this.gradeService.findAllByCourseId(courseId));
            model.addAttribute("courses", this.courseService.listAllCourses());
            model.addAttribute("students", this.studentService.listAll());
            return "grades";
        }
        else{
            model.addAttribute("grades1", this.gradeService.findAllByUsername(username));
            model.addAttribute("courses", this.courseService.listAllCourses());
            model.addAttribute("students", this.studentService.listAll());
            return "grades";
        }

    }

    @GetMapping("/edit-grade")
    public String getEditGradePage(@RequestParam String username,
                                   @RequestParam Long courseId,
                                   @RequestParam Character gradeCharacter,
                                   Model model){
        Grade grade;
        if(gradeCharacter != null){
            grade = this.gradeService.findByUsernameAndCourseId(username, courseId);
        }
        else
            grade = this.gradeService.addGrade(username, courseId);

        model.addAttribute("grade", grade);
        return "edit-grade";
    }

    @PostMapping("/edit-grade")
    public String saveEditedGrade(HttpServletRequest request, Model model){
        Long id = Long.parseLong(request.getParameter("gradeId"));
        char c = request.getParameter("grade").charAt(0);
        String username = request.getParameter("username");
        Long courseId = Long.parseLong(request.getParameter("courseId"));
        this.gradeService.changeGrade(id, c, username, courseId);
        model.addAttribute("course", this.courseService.findCourseById(courseId).orElseThrow(()->new CourseNotFoundException(courseId)));
        model.addAttribute("grades", this.gradeService.getGradesWithMap(courseId));
        return "studentsInCourse";
    }
}
