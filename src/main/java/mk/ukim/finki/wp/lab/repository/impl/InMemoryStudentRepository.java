//package mk.ukim.finki.wp.lab.repository.impl;
//
//import mk.ukim.finki.wp.lab.model.Course;
//import mk.ukim.finki.wp.lab.model.Student;
//import org.springframework.stereotype.Repository;
//
//import javax.annotation.PostConstruct;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.stream.Collectors;
//
//@Repository
//public class InMemoryStudentRepository {
//
//    private List<Student> studentList = new ArrayList<>();
//
//    @PostConstruct
//    public void init(){
//        studentList.add(new Student("jordan.ivanov", "ji", "Jordan",  "Ivanov"));
//        studentList.add(new Student("marija.shirikj", "ms", "Marija",  "Shirikj"));
//        studentList.add(new Student("todor.nikolov", "tn", "Todor",  "Nikolov"));
//        studentList.add(new Student("kosta.spasov", "ks", "Kosta",  "Spasov"));
//        studentList.add(new Student("goran.jovanov", "gj", "Goran",  "Jovanov"));
//    }
//
//    public List<Student> findAllStudents(){
//        return studentList;
//    }
//
//    public List<Student> findAllByNameOrSurname(String text){
//        return studentList.stream().filter(r->r.getName().equals(text) || r.getSurname().equals(text)).collect(Collectors.toList());
//    }
//
//    public Student findStudentByUsername(String username){
//        return studentList.stream().filter(r->r.getUsername().equals(username)).findFirst().get();
//    }
//
//    public Student saveOrUpdate(Student student) {
//        studentList.removeIf(r->r.getUsername().equals(student.getUsername()));
//        studentList.add(student);
//        return student;
//    }
//
//    public Student addCourseToStudent(Student student, Course course){
//        student.getStudentCourses().removeIf(c->c.getCourseId()==course.getCourseId());
//        student.getStudentCourses().add(course);
//        return student;
//    }
//}
