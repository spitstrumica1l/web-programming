package mk.ukim.finki.wp.lab;

import mk.ukim.finki.wp.lab.model.Student;
import mk.ukim.finki.wp.lab.model.exceptions.InvalidArgumentsException;
import mk.ukim.finki.wp.lab.model.exceptions.StudentAlreadyPresentException;
import mk.ukim.finki.wp.lab.repository.jpa.CourseRepository;
import mk.ukim.finki.wp.lab.repository.jpa.StudentRepository;
import mk.ukim.finki.wp.lab.service.StudentService;
import mk.ukim.finki.wp.lab.service.impl.StudentServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class SaveStudentTest {

    @Mock
    private CourseRepository courseRepository;
    @Mock
    private StudentRepository studentRepository;

    private StudentService service;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        Student student = new Student("username", "password", "name", "surname");
        Mockito.when(this.studentRepository.save(Mockito.any(Student.class))).thenReturn(student);

        service = Mockito.spy(new StudentServiceImpl(this.studentRepository, this.courseRepository));
    }

    @Test
    public void testSuccessSaving(){
        Student student = this.service.save("username", "password", "name", "surname");

        Mockito.verify(this.service).save("username", "password", "name", "surname");

        Assert.assertNotNull("Student is null", student);

        Assert.assertEquals("username do not match", "username", student.getUsername());
        Assert.assertEquals("password do not match", "password", student.getPassword());
        Assert.assertEquals("name do not match", "name", student.getName());
        Assert.assertEquals("surname do not match", "surname", student.getSurname());
    }

    @Test
    public void testEmptyUsername(){
        String username = "";
        Assert.assertThrows("InvalidArgumentsException expected",
                InvalidArgumentsException.class,
                () -> this.service.save(username,"password","name", "surname"));
        Mockito.verify(this.service).save(username, "password","name", "surname");
    }

    @Test
    public void testEmptyPassword(){
        String username = "username";
        String password = "";
        Assert.assertThrows("InvalidArgumentsException expected",
                InvalidArgumentsException.class,
                () -> this.service.save(username,password, "name", "surname"));
        Mockito.verify(this.service).save(username, password, "name", "surname");
    }

    @Test
    public void testNullPassword(){
        String username = "username";
        String password = null;
        Assert.assertThrows("InvalidArgumentsException expected",
                InvalidArgumentsException.class,
                () -> this.service.save(username,password, "name", "surname"));
        Mockito.verify(this.service).save(username, password, "name", "surname");
    }

    @Test
    public void testDupicateUsername(){
        Student student = new Student("username", "password", "name", "surname");
        Mockito.when(this.studentRepository.findByUsername(Mockito.anyString())).thenReturn(Optional.of(student));
        String username = "username";
        Assert.assertThrows("StudentAlreadyPresentException expected",
                StudentAlreadyPresentException.class,
                () -> this.service.save(username, "password", "name", "surname"));
        Mockito.verify(this.service).save(username, "password", "name", "surname");
    }
}
