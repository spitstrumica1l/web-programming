package mk.ukim.finki.wp.lab.selenium;

import lombok.Getter;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

@Getter
public class CoursesPage extends AbstractPage{

//    додавање, уредување и бришење

    @FindBy(css = ".add")
    private List<WebElement> addCourseButton;

    @FindBy(className = "edit")
    private List<WebElement> editCourseButtons;

    @FindBy(className = "delete")
    private List<WebElement> deleteCourseButtons;

    public CoursesPage(WebDriver driver) {
        super(driver);
    }

    public static CoursesPage to(WebDriver driver){
        get(driver, "/courses");
        return PageFactory.initElements(driver, CoursesPage.class);
    }

    public void assertElements(int addButtons, int editButtons, int deleteButtons){
        Assert.assertEquals("add is visible", addButtons, this.getAddCourseButton().size());
        Assert.assertEquals("edit do not match", editButtons, this.getEditCourseButtons().size());
        Assert.assertEquals("delete do not match", deleteButtons, this.getDeleteCourseButtons().size());
    }
}
