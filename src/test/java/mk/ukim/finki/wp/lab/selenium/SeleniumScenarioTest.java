package mk.ukim.finki.wp.lab.selenium;

import mk.ukim.finki.wp.lab.model.Course;
import mk.ukim.finki.wp.lab.model.Teacher;
import mk.ukim.finki.wp.lab.service.CourseService;
import mk.ukim.finki.wp.lab.service.TeacherService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class SeleniumScenarioTest {

    private HtmlUnitDriver driver;

    @Autowired
    private CourseService courseService;

    @Autowired
    private TeacherService teacherService;

    private static Course course1, course2;
    private static Teacher teacher1;
    private static boolean dataInitialized = false;

    @BeforeEach
    private void setup(){
        this.driver = new HtmlUnitDriver(true);
        initData();
    }

    @AfterEach
    public void destroy(){
        if(this.driver != null)
            this.driver.close();
    }

    private void initData() {
        if (!dataInitialized) {
            teacher1 = this.teacherService.save("teacher", "1");

            course1 = courseService.saveCourse(null, "course 1", "course 1 description", teacher1.getId());
            course2 = courseService.saveCourse(null, "course 2", "course 2 description", teacher1.getId());

           dataInitialized = true;
        }
    }

    @Test
    public void testScenario() {
        CoursesPage coursesPage = CoursesPage.to(this.driver);
        Assert.assertEquals("url do not match", "http://localhost:9999/courses", this.driver.getCurrentUrl());
        coursesPage.assertElements(0,0,0);

        LoginPage loginPage = LoginPage.openLogin(this.driver);

        coursesPage = LoginPage.doLogin(driver, loginPage, "admin", "admin");
        coursesPage.assertElements(1,2,2);

        coursesPage =AddOrEditCourse.addCourse(driver, "test1", "testt11", teacher1.getName() + ' ' +  teacher1.getSurname());
        coursesPage.assertElements(1,3,3);

        coursesPage =AddOrEditCourse.addCourse(driver, "test2", "testt21", teacher1.getName() + ' ' +  teacher1.getSurname());
        coursesPage.assertElements(1,4,4);

        coursesPage.getDeleteCourseButtons().get(1).click();
        coursesPage.assertElements(1,4,4);

        coursesPage = AddOrEditCourse.editCourse(this.driver, coursesPage.getEditCourseButtons().get(0), "test100", "t10000", teacher1.getName() + ' ' +  teacher1.getSurname());
        coursesPage.assertElements(1,4,4);

        loginPage = LoginPage.logout(this.driver);
        coursesPage = CoursesPage.to(this.driver);
        Assert.assertEquals("url do not match", "http://localhost:9999/courses", this.driver.getCurrentUrl());
        coursesPage.assertElements(0,0,0);
    }


}
