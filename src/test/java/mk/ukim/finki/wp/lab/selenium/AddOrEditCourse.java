package mk.ukim.finki.wp.lab.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddOrEditCourse extends AbstractPage{

    private WebElement name;
    private WebElement description;
    private WebElement teacher;
    @FindBy(css = ".submit")
    private WebElement submit;

    public AddOrEditCourse(WebDriver driver) {
        super(driver);
    }

    public static CoursesPage addCourse(WebDriver driver, String name, String description, String teacher){
        get(driver, "/courses/add-form");
        AddOrEditCourse addOrEditCourse = PageFactory.initElements(driver, AddOrEditCourse.class);
        addOrEditCourse.name.sendKeys(name);
        addOrEditCourse.description.sendKeys(description);
        addOrEditCourse.teacher.click();
        addOrEditCourse.teacher.findElement(By.xpath("//option[. = '" + teacher + "']")).click();

        addOrEditCourse.submit.click();
        return PageFactory.initElements(driver, CoursesPage.class);
    }

    public static CoursesPage editCourse(HtmlUnitDriver driver, WebElement webElement, String name, String description, String teacher) {
        webElement.click();
        AddOrEditCourse addOrEditCourse =PageFactory.initElements(driver, AddOrEditCourse.class);
        addOrEditCourse.name.sendKeys(name);
        addOrEditCourse.description.sendKeys(description);
        addOrEditCourse.teacher.click();
        addOrEditCourse.teacher.findElement(By.xpath("//option[. = '" + teacher + "']")).click();

        addOrEditCourse.submit.click();
        return PageFactory.initElements(driver, CoursesPage.class);
    }
}
